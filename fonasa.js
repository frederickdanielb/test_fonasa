// DATOS PARA TRABAJAR
const hospital = [
    { id: 1, name: 'HOSPITAL SANTIAGO' },
    { id: 2, name: 'HOSPITAL PROVIDENCIA' },
    { id: 3, name: 'HOSPITAL LAS CONDES' }
];
const consulta = [
    { id: 1, hospitalId: 1, cantPacientes: 3, nombreEspecialista: 'Camila', tipoConsulta: 1, estado: 1},
    { id: 2, hospitalId: 2, cantPacientes: 6, nombreEspecialista: 'Pedro', tipoConsulta: 2, estado: 2 },
    { id: 3, hospitalId: 1, cantPacientes: 10, nombreEspecialista: 'Javiera', tipoConsulta: 3, estado: 2},
    { id: 4, hospitalId: 3, cantPacientes: 2, nombreEspecialista: 'Jose', tipoConsulta: 1, estado: 1},
]

const paciente = [
    { id: 1, nroHistoriaClinica: 1001, edad: 36, nombre: 'Luis', tieneDieta: true, esFumador: true, aniosFumando: 5, relacionPesoEstatura: 4 },
    { id: 2, nroHistoriaClinica: 1001, edad: 6, nombre: 'Eduardo', tieneDieta: false, esFumador: false, aniosFumando: 0 ,relacionPesoEstatura: 2 },
    { id: 3, nroHistoriaClinica: 1001, edad: 14, nombre: 'Maria', tieneDieta: true, esFumador: true, aniosFumando: 1 , relacionPesoEstatura: 2 },
    { id: 4, nroHistoriaClinica: 1004, edad: 65, nombre: 'Eugenia', tieneDieta: false, esFumador: true, aniosFumando: 20, relacionPesoEstatura: 1 },
]

const tipoConsulta = [
    { id: 1, name: 'Pediatria' },
    { id: 2, name: 'Urgencia' },
    { id: 3, name: 'CGI (Consulta General Integral)).' }
];
const estado = [
    { id: 1, name: 'Ocupado' },
    { id: 2, name: 'En espera' }
];

//Clase encargada del calculo correspondiente aplicado a los pacientes
class Calculo {
    constructor(paciente, prioridad = null) {
        //Miembros
        this.pacienteProp = paciente;
        this.prioridadProp = prioridad;
    }
    //Propiedad prioridadGet
    get prioridadGet() {
        return this.prioridadCalc(this.pacienteProp);
    }
    //Propiedad riesgoGet
    get riesgoGet() {
        return this.riesgoCalc(this.pacienteProp, this.prioridadProp);
    }

    /**
    * Calcula el riesgo del paciente
    * @param  {array} paciente
    * @param  {numeric} prioridad
    * @return  {numeric}
    */
    riesgoCalc(paciente, prioridad) {
        if (paciente.edad >= 1 && paciente.edad <= 40) {
            return (paciente.edad * prioridad ) / 100;
        }
        return (paciente.edad * prioridad) / 5.3;
    }

    /**
    * Calcula la prioridad del paciente
    * @param  {array} paciente
    * @return  {numeric}
    */
    prioridadCalc(paciente) {
        //caso ninos
        if (paciente.edad >= 1 && paciente.edad  <= 15){
            if (paciente.edad >= 1 && paciente.edad <= 5)
                return paciente.relacionPesoEstatura + 3;
            if (paciente.edad >= 6 && paciente.edad <= 12)
                return paciente.relacionPesoEstatura + 2;

            if (paciente.edad >= 13 && paciente.edad <= 15)
                return paciente.relacionPesoEstatura + 1;
        }
        //caso joven
        if (paciente.edad >= 16 && paciente.edad <= 40) {
            if (paciente.esFumador ){
                return (paciente.aniosFumando / 4) + 2;
            }
            return 2;
        }
        //caso anciano
        if (paciente.edad >= 41 ) {
            if (paciente.tieneDieta && paciente.edad >= 60 && paciente.edad <= 100) {
                return (paciente.edad / 20) + 4;
            }
            return (paciente.edad / 30) + 3;
        }
    }
}


// Listado de pacientes con riesgo y prioridad calculado
function listPacientes() {
    return paciente.map((pacientes) => {
        let initPrioridad = new Calculo(pacientes,null);
        let initRiesgo = new Calculo(pacientes, initPrioridad.prioridadGet);
        return {
            ...pacientes,
            prioridad: initPrioridad.prioridadGet,
            riesgo: initRiesgo.riesgoGet
        }
    });
};

// Listar_Pacientes_Mayor_Riesgo.
function Listar_Pacientes_Mayor_Riesgo(nroFichaClinica) {
    const pacientes = listPacientes()
    return pacientes.filter((paciente) => paciente.nroHistoriaClinica == nroFichaClinica)
    .sort((pacienteA, pacienteB) => {
        return (pacienteA.riesgo < pacienteB.riesgo ? 1 : (pacienteA.riesgo >pacienteB.riesgo ? -1 : 0))
    })
    .map((paciente) => {
        return {
            nombre: paciente.nombre,
            riesgo: paciente.riesgo 
        }
    });
};

// Listar_Pacientes_Fumadores_Urgentes.
function Listar_Pacientes_Fumadores_Urgentes() {
    const pacientes = listPacientes()
    return pacientes.filter((paciente) => paciente.esFumador)
        .sort((pacienteA, pacienteB) => { 
            return (pacienteA.prioridad < pacienteB.prioridad ? 1 : (pacienteA.prioridad > pacienteB.prioridad ? -1 : 0)) 
        })
        .map((paciente) => { 
            return { 
                nombre: paciente.nombre, 
                prioridad: paciente.prioridad
            } 
        });
};

//Consulta_mas_Pacientes_Atendidos
function Consulta_mas_Pacientes_Atendidos(){
    return consulta.sort((consultaA,consultaB)=>{
        return (consultaA.cantPacientes < consultaB.cantPacientes ? 1 : consultaA.cantPacientes > consultaB.cantPacientes ? -1 : 0)
    })
    .filter((consulta, index)=> index === 0)
    .map((consulta)=>{
        return{
            ID: consulta.id,
            Especialista: consulta.nombreEspecialista,
            cantPacientes : consulta.cantPacientes,
            Hospital : hospital.filter((hospital)=>hospital.id === consulta.hospitalId).map((hospital)=> hospital.name)[0],
            tipoConsulta : tipoConsulta.filter((tipoConsulta)=>tipoConsulta.id === consulta.tipoConsulta).map((tipoConsulta)=> tipoConsulta.name)[0],
            Estado : estado.filter((estado)=>estado.id === consulta.estado).map((estado)=> estado.name)[0]
        }
    })
}






// Impresión de soluciones. 
console.log('---------------LISTA DE PACIENTE------------------------------------------');
console.log(listPacientes());




console.log('---------------LISTADO DE PACIENTES CON MAYOR RIESGO------------------------------------------');
console.log(Listar_Pacientes_Mayor_Riesgo(1001));




console.log('---------------LISTADO DE PACIENTES FUMADORES------------------------------------------');
console.log(Listar_Pacientes_Fumadores_Urgentes());




console.log('---------------CONSULTA CON MAYOR PACIENTES ATENDIDOS------------------------------------------');
console.log(Consulta_mas_Pacientes_Atendidos());